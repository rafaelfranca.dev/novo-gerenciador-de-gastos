package com.rafael.gerenciador.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@Configuration
@ComponentScan
@SpringBootApplication
public class NovoGerenciadorDeGastosApplication {

	public static void main(String[] args) {
		SpringApplication.run(NovoGerenciadorDeGastosApplication.class, args);
	}

}
